using Microsoft.Extensions.DependencyInjection;

namespace Codebrudi.CoCo.Contract.Bootstrapping;

public interface IBootstrapper
{
    void ActivatingAll();
    void ActivatedAll();
    void DeactivatedAll();
    void DeactivatingAll();
    void RegisterAllMappings(IServiceCollection kernel);
}
