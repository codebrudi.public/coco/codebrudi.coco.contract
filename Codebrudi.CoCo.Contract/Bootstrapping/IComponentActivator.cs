using Microsoft.Extensions.DependencyInjection;

namespace Codebrudi.CoCo.Contract.Bootstrapping;

public interface IComponentActivator
{
    void Activating();
    void Activated();
    void Deactivating();
    void Deactivated();
    void RegisterMappings(IServiceCollection services);
}
